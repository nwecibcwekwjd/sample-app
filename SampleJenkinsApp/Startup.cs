﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SampleJenkinsApp.Startup))]
namespace SampleJenkinsApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
